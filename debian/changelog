jedit (5.5.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Update Standards-Version to 4.5.1
  * Use debhelper-compat.
    - Update compat level to 13.
  * Update Vcs to salsa.
  * Add Rules-Requires-Root: no
  * Remove override for removed lintian tag.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Thu, 31 Dec 2020 17:01:34 +0000

jedit (5.5.0+dfsg-1) unstable; urgency=medium

  * New upstream version 5.5.0+dfsg
  * Add repacksuffix to debian/watch
  * Drop get-orig-source target (Policy 4.1.4)
  * Bump Standards-Version to 4.1.4

 -- tony mancill <tmancill@debian.org>  Sun, 22 Apr 2018 09:43:35 -0700

jedit (5.4.0+dfsg-3) unstable; urgency=medium

  * Team upload.
  * Make the ant clean-all call conditional due to the weird
    dh_quilt_patch hack. Otherwise the java9.patch is not applied when
    dh_auto_clean is executed. (Closes: #893210)
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.1.3.

 -- Markus Koschany <apo@debian.org>  Thu, 22 Mar 2018 15:55:01 +0100

jedit (5.4.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Declare compliance with Debian Policy 4.1.1.
  * Use canonical VCS address.
  * Remove override for dh_builddeb because xz is the default compressor now.
  * Remove source/options file because xz is the default now.
  * Fix FTBFS with OpenJDK 9.
    Thanks to Chris West for the report. (Closes: #875583)
  * Require at least Java 8 for compilation.
  * Fix jedit.sh script and require at least a Java 8 runtime.

 -- Markus Koschany <apo@debian.org>  Wed, 29 Nov 2017 22:54:30 +0100

jedit (5.4.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version updated to 3.9.8 (no changes)
  * Remove Gabriele Giacone from Uploaders. (Closes: #856759)
    - Thank you for your contributions to the package.
  * Update debian/watch and debian/copyright to exclude bsh.

 -- tony mancill <tmancill@debian.org>  Tue, 08 Aug 2017 07:12:19 -0700

jedit (5.3.0+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.
  * Update debian/copyright.

 -- tony mancill <tmancill@debian.org>  Sun, 03 Jan 2016 18:30:58 -0800

jedit (5.2.0+dfsg-2) unstable; urgency=medium

  * Drop the .menu file, since jedit ships a .desktop file.
    This is now prohibitted by CTTE #741573.
  * Depend on Java 7 runtime.  Build-dep on JDK 7.
  * Address lintian warning in d/copyright.

 -- tony mancill <tmancill@debian.org>  Sat, 17 Oct 2015 21:57:05 -0700

jedit (5.2.0+dfsg-1) unstable; urgency=medium

  * New upstream version.

 -- tony mancill <tmancill@debian.org>  Thu, 08 Oct 2015 21:39:29 -0700

jedit (5.1.0+dfsg-2) unstable; urgency=medium

  * Team upload.
  * Added an explicit build dependency on libxerces2-java since it's no longer
    on the Ant classpath (Closes: #765056)
  * Standards-Version updated to 3.9.6 (no changes)
  * Switch to debhelper level 9
  * debian/copyright: Fixed the short license names

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 13 Oct 2014 12:27:37 +0200

jedit (5.1.0+dfsg-1) unstable; urgency=low

  * New upstream version.
  * Update debian/copyright with license for usr/share/jedit/macros/Emacs
    - update debian/rules to exclude LICENSE.md file

 -- tony mancill <tmancill@debian.org>  Sun, 20 Oct 2013 21:35:04 -0700

jedit (5.0.0+dfsg-4) unstable; urgency=low

  * Add non-virtual "default-jre" as the first JRE dependency.
    - Thanks to Eric Lavarde.  (Closes: #726879)
  * Add lintian override for license-problem-gfdl-invariants
    - doc/FAQ/faq.xml is GDFL, but "with no Invariant Sections"

 -- tony mancill <tmancill@debian.org>  Sun, 20 Oct 2013 13:01:04 -0700

jedit (5.0.0+dfsg-3) unstable; urgency=low

  * Depend on any JRE that provides java[67]-runtime (Closes: #724620)
  * Add myself to uploaders.
  * Update Vcs URLs in debian/control to be canonical.

 -- tony mancill <tmancill@debian.org>  Wed, 25 Sep 2013 22:11:38 -0700

jedit (5.0.0+dfsg-2) unstable; urgency=low

  [ Gabriele Giacone ]
  * Team upload.
  * B-D: Replace openjdk|sunjdk with default-jdk (Closes: #683502).
  * Fix icons gamma correction.

  [ tony mancill ]
  * Bump Standards-Version to 3.9.4.
  * Remove deprectated DMUA flag field.
  * No longer ask java-wrappers to search for java5 (Closes: #606662)
    - Note, this bug was addressed in a previous upload, but it's not
      clear from the changelog or VCS history when.

 -- tony mancill <tmancill@debian.org>  Sun, 02 Jun 2013 07:27:24 -0700

jedit (5.0.0+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Add libjsr305-java to B-D.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sun, 24 Mar 2013 20:40:33 +0100

jedit (5.0~pre1+dfsg-1) experimental; urgency=low

  * New upstream release.
  * Add junit to B-D.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sat, 16 Jun 2012 09:24:44 +0000

jedit (4.5.2+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Sources moved from jEdit/ dir to root.
  * Bump Standards-Version to 3.9.3 (no changes).
  * Add ant-contrib, bsh, docbook-xsl-saxon, libbsf-java,
    libcommons-logging-java to B-D.
  * Switch source and binary to xz compression.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Fri, 15 Jun 2012 14:35:42 +0200

jedit (4.4.2+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Fix pixmaps dir typo (Closes: #648232).
  * Fix d/copyright according to DEP-5.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Thu, 10 Nov 2011 00:50:56 +0100

jedit (4.4.1+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.2.
  * Add docbook-xml as B-D.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Fri, 12 Aug 2011 22:14:44 +0200

jedit (4.3.2+dfsg-3) unstable; urgency=low

  * Updated bsh-src version required (Closes: #591153).
  * Standards version to 3.9.1.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Sun, 01 Aug 2010 12:22:54 +0200

jedit (4.3.2+dfsg-2) unstable; urgency=low

  * Added desktop menu, Debian menu and icons from upstream.
    Thanks to Daniel Hahler msgid:<201006162350.30109.ubuntu@thequod.de>

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Fri, 18 Jun 2010 01:44:33 +0200

jedit (4.3.2+dfsg-1) unstable; urgency=low

  * New upstream release.
  * Added java-wrappers minimum required version.

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Fri, 21 May 2010 23:44:08 +0200

jedit (4.3.1+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #136535)

 -- Gabriele Giacone <1o5g4r8o@gmail.com>  Mon, 12 Apr 2010 01:09:45 +0200
