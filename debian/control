Source: jedit
Section: editors
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: tony mancill <tmancill@debian.org>
Build-Depends: ant,
               ant-contrib,
               ant-optional,
               bsh,
               bsh-src (>= 2.0b4-11),
               debhelper-compat (= 13),
               docbook-xml,
               docbook-xsl,
               docbook-xsl-saxon,
               imagemagick,
               javahelper (>= 0.24),
               junit4,
               libbsf-java,
               libcommons-logging-java,
               libjsr305-java,
               libxerces2-java,
               quilt (>= 0.46-7~),
               xsltproc
Build-Conflicts: junit
Build-Depends-Indep: default-jdk (>= 2:1.8)
Standards-Version: 4.5.1
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/java-team/jedit.git
Vcs-Browser: https://salsa.debian.org/java-team/jedit
Homepage: http://www.jedit.org/

Package: jedit
Architecture: all
Pre-Depends: dpkg (>= 1.15.6~)
Depends: java-wrappers (>= 0.1.14),
         default-jre (>= 2:1.7) | java7-runtime,
         ${misc:Depends}
Description: Plugin-based editor for programmers
 As one of the most feature rich editors available, jEdit boasts support for
 syntax highlighting in more than 140 languages. jEdit combines the power of
 Emacs, the user-friendliness of Kate, and the advanced editing features
 (such as vertical paste) of Ultraedit, to bring you an open-source
 plugin-based programmer's editor of professional quality.
 .
 It is possible to define complex macros in BeanShell or Jython, or other
 languages that fit into the BSF. jEdit offers a powerful and user-friendly
 keyboard mapping system (including 2-keystroke shortcuts), making it
 possible to give jEdit a very Emacs-like feel, if you so desire.
 .
 Its functionality is easily extended by the use of 'plugins' which can be
 downloaded, updated, and installed, all without exiting the editor. These
 include a built-in Console shell integration, which lets you execute
 interactive external commands inside your editor, as well as bind them to
 keyboard shortcuts. The FTP plugin lets you browse and edit files on remote
 systems over FTP or SFTP. Other plugins provide shells, object oriented
 structure/code browsers, or completion popups for Java, XML, HTML, Ant,
 LaTeX, Python, Ruby, Perl, C, C++, bash, Scheme, Prolog, and many other
 languages.
