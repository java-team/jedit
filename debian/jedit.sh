#!/bin/sh
#
#
. /usr/lib/java-wrappers/java-wrappers.sh

find_java_runtime java8

find_jars /usr/share/jedit/jedit.jar

run_java org.gjt.sp.jedit.jEdit "$@"
